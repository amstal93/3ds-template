/*
 * 3DS Template
 */

#include <cstdint>
using u8 = uint8_t;
using u32 = uint32_t;
#include "kitten_t3x.h"
#include "vshader_shbin.h"
#include <3ds.h>
#include <citro3d.h>
#include <string.h>
#include <tex3ds.h>

constexpr auto CLEAR_COLOR{0x68B0D8FF};

constexpr auto DISPLAY_TRANSFER_FLAGS{GX_TRANSFER_FLIP_VERT(0) | GX_TRANSFER_OUT_TILED(0) | GX_TRANSFER_RAW_COPY(0) |
                                      GX_TRANSFER_IN_FORMAT(GX_TRANSFER_FMT_RGBA8) |
                                      GX_TRANSFER_OUT_FORMAT(GX_TRANSFER_FMT_RGB8) |
                                      GX_TRANSFER_SCALING(GX_TRANSFER_SCALE_NO)};

struct Vertex
{
        float position[3];
        float tex_coord[2];
        float normal[3];
};

constexpr Vertex CUBE_VERTICES[]{
    // First face (PZ)
    // First triangle
    {.position = {-0.5f, -0.5f, +0.5f}, .tex_coord = {0.0f, 0.0f}, .normal = {0.0f, 0.0f, +1.0f}},
    {{+0.5f, -0.5f, +0.5f}, {1.0f, 0.0f}, {0.0f, 0.0f, +1.0f}},
    {{+0.5f, +0.5f, +0.5f}, {1.0f, 1.0f}, {0.0f, 0.0f, +1.0f}},
    // Second triangle
    {{+0.5f, +0.5f, +0.5f}, {1.0f, 1.0f}, {0.0f, 0.0f, +1.0f}},
    {{-0.5f, +0.5f, +0.5f}, {0.0f, 1.0f}, {0.0f, 0.0f, +1.0f}},
    {{-0.5f, -0.5f, +0.5f}, {0.0f, 0.0f}, {0.0f, 0.0f, +1.0f}},

    // Second face (MZ)
    // First triangle
    {{-0.5f, -0.5f, -0.5f}, {0.0f, 0.0f}, {0.0f, 0.0f, -1.0f}},
    {{-0.5f, +0.5f, -0.5f}, {1.0f, 0.0f}, {0.0f, 0.0f, -1.0f}},
    {{+0.5f, +0.5f, -0.5f}, {1.0f, 1.0f}, {0.0f, 0.0f, -1.0f}},
    // Second triangle
    {{+0.5f, +0.5f, -0.5f}, {1.0f, 1.0f}, {0.0f, 0.0f, -1.0f}},
    {{+0.5f, -0.5f, -0.5f}, {0.0f, 1.0f}, {0.0f, 0.0f, -1.0f}},
    {{-0.5f, -0.5f, -0.5f}, {0.0f, 0.0f}, {0.0f, 0.0f, -1.0f}},

    // Third face (PX)
    // First triangle
    {{+0.5f, -0.5f, -0.5f}, {0.0f, 0.0f}, {+1.0f, 0.0f, 0.0f}},
    {{+0.5f, +0.5f, -0.5f}, {1.0f, 0.0f}, {+1.0f, 0.0f, 0.0f}},
    {{+0.5f, +0.5f, +0.5f}, {1.0f, 1.0f}, {+1.0f, 0.0f, 0.0f}},
    // Second triangle
    {{+0.5f, +0.5f, +0.5f}, {1.0f, 1.0f}, {+1.0f, 0.0f, 0.0f}},
    {{+0.5f, -0.5f, +0.5f}, {0.0f, 1.0f}, {+1.0f, 0.0f, 0.0f}},
    {{+0.5f, -0.5f, -0.5f}, {0.0f, 0.0f}, {+1.0f, 0.0f, 0.0f}},

    // Fourth face (MX)
    // First triangle
    {{-0.5f, -0.5f, -0.5f}, {0.0f, 0.0f}, {-1.0f, 0.0f, 0.0f}},
    {{-0.5f, -0.5f, +0.5f}, {1.0f, 0.0f}, {-1.0f, 0.0f, 0.0f}},
    {{-0.5f, +0.5f, +0.5f}, {1.0f, 1.0f}, {-1.0f, 0.0f, 0.0f}},
    // Second triangle
    {{-0.5f, +0.5f, +0.5f}, {1.0f, 1.0f}, {-1.0f, 0.0f, 0.0f}},
    {{-0.5f, +0.5f, -0.5f}, {0.0f, 1.0f}, {-1.0f, 0.0f, 0.0f}},
    {{-0.5f, -0.5f, -0.5f}, {0.0f, 0.0f}, {-1.0f, 0.0f, 0.0f}},

    // Fifth face (PY)
    // First triangle
    {{-0.5f, +0.5f, -0.5f}, {0.0f, 0.0f}, {0.0f, +1.0f, 0.0f}},
    {{-0.5f, +0.5f, +0.5f}, {1.0f, 0.0f}, {0.0f, +1.0f, 0.0f}},
    {{+0.5f, +0.5f, +0.5f}, {1.0f, 1.0f}, {0.0f, +1.0f, 0.0f}},
    // Second triangle
    {{+0.5f, +0.5f, +0.5f}, {1.0f, 1.0f}, {0.0f, +1.0f, 0.0f}},
    {{+0.5f, +0.5f, -0.5f}, {0.0f, 1.0f}, {0.0f, +1.0f, 0.0f}},
    {{-0.5f, +0.5f, -0.5f}, {0.0f, 0.0f}, {0.0f, +1.0f, 0.0f}},

    // Sixth face (MY)
    // First triangle
    {{-0.5f, -0.5f, -0.5f}, {0.0f, 0.0f}, {0.0f, -1.0f, 0.0f}},
    {{+0.5f, -0.5f, -0.5f}, {1.0f, 0.0f}, {0.0f, -1.0f, 0.0f}},
    {{+0.5f, -0.5f, +0.5f}, {1.0f, 1.0f}, {0.0f, -1.0f, 0.0f}},
    // Second triangle
    {{+0.5f, -0.5f, +0.5f}, {1.0f, 1.0f}, {0.0f, -1.0f, 0.0f}},
    {{-0.5f, -0.5f, +0.5f}, {0.0f, 1.0f}, {0.0f, -1.0f, 0.0f}},
    {{-0.5f, -0.5f, -0.5f}, {0.0f, 0.0f}, {0.0f, -1.0f, 0.0f}},
};

constexpr auto CUBE_VERTICES_COUNT{sizeof(CUBE_VERTICES) / sizeof(CUBE_VERTICES[0])};

static DVLB_s *vshader_dvlb;
static shaderProgram_s shader_program;
static int uloc_projection, uloc_model_view;
static int uloc_light_vec, uloc_light_half_vec, uloc_light_clr, uloc_material;
static C3D_Mtx projection;
constexpr C3D_Mtx MATERIAL{{
    {{0.0f, 0.2f, 0.2f, 0.2f}}, // Ambient
    {{0.0f, 0.4f, 0.4f, 0.4f}}, // Diffuse
    {{0.0f, 0.8f, 0.8f, 0.8f}}, // Specular
    {{1.0f, 0.0f, 0.0f, 0.0f}}, // Emission
}};

static void *vbo_data;
static C3D_Tex kitten_tex;
static auto angle_x{0.0f};
static auto angle_y{0.0f};

static C3D_RenderTarget *target_left;
static C3D_RenderTarget *target_right; // AUTOSTEREOSCOPIC 3D

/*!
 * In case you do not want to use exceptions but return codes, use [[nodiscard]] so that callers cannot ignore the
 * return value.
 */
[[nodiscard]] auto load_texture_from_mem(C3D_Tex *tex, C3D_TexCube *cube, const void *data, size_t size)
{
        if (auto t3x = Tex3DS_TextureImport(data, size, tex, cube, false))
        {
                Tex3DS_TextureFree(t3x); // Delete the t3x object since we don't need it
                return true;
        }
        else
        {
                return false;
        }
}

/*!
 * Load the vertex shader, create a shader program and bind it
 */
auto load_vertex_shader()
{
        vshader_dvlb = DVLB_ParseFile((u32 *)vshader_shbin, vshader_shbin_size);
        shaderProgramInit(&shader_program);
        shaderProgramSetVsh(&shader_program, &vshader_dvlb->DVLE[0]);
        C3D_BindProgram(&shader_program);
}

auto determine_location_of_uniforms()
{
        uloc_projection = shaderInstanceGetUniformLocation(shader_program.vertexShader, "projection");
        uloc_model_view = shaderInstanceGetUniformLocation(shader_program.vertexShader, "modelView");
        uloc_light_vec = shaderInstanceGetUniformLocation(shader_program.vertexShader, "lightVec");
        uloc_light_half_vec = shaderInstanceGetUniformLocation(shader_program.vertexShader, "lightHalfVec");
        uloc_light_clr = shaderInstanceGetUniformLocation(shader_program.vertexShader, "lightClr");
        uloc_material = shaderInstanceGetUniformLocation(shader_program.vertexShader, "material");
}

/*!
 * Configure attributes for use with the vertex shader
 */
auto configure_shader_attributes()
{
        const auto attr_info{C3D_GetAttrInfo()};
        AttrInfo_Init(attr_info);
        AttrInfo_AddLoader(attr_info, 0, GPU_FLOAT, 3); // v0=position
        AttrInfo_AddLoader(attr_info, 1, GPU_FLOAT, 2); // v1=tex_coord
        AttrInfo_AddLoader(attr_info, 2, GPU_FLOAT, 3); // v2=normal
}

/*!
 * VBO = vertex buffer object
 */
auto populate_cube_vbo()
{
        vbo_data = linearAlloc(sizeof(CUBE_VERTICES));
        memcpy(vbo_data, CUBE_VERTICES, sizeof(CUBE_VERTICES));
}

auto configure_vbo_buffer()
{
        const auto buf_info{C3D_GetBufInfo()};
        BufInfo_Init(buf_info);
        BufInfo_Add(buf_info, vbo_data, sizeof(Vertex), 3, 0x210);
}

/*!
 * Load the texture and bind it to the first texture unit
 */
auto populate_first_texture_unit()
{
        if (load_texture_from_mem(&kitten_tex, nullptr, kitten_t3x, kitten_t3x_size))
        {
                C3D_TexSetFilter(&kitten_tex, GPU_LINEAR, GPU_NEAREST);
                C3D_TexBind(0, &kitten_tex);
                // Configure the first fragment shading substage to blend the texture color with
                // the vertex color (calculated by the vertex shader using a lighting algorithm)
                // See https://www.opengl.org/sdk/docs/man2/xhtml/glTexEnv.xml for more insight
                const auto env{C3D_GetTexEnv(0)};
                C3D_TexEnvInit(env);
                C3D_TexEnvSrc(env, C3D_Both, GPU_TEXTURE0, GPU_PRIMARY_COLOR, GPU_TEVSRC::GPU_PRIMARY_COLOR);
                C3D_TexEnvFunc(env, C3D_Both, GPU_MODULATE);
        }
        else
        {
                svcBreak(USERBREAK_PANIC); // This is actually an EXIT!
        }
}

auto initialize_scene()
{
        load_vertex_shader();
        determine_location_of_uniforms();
        configure_shader_attributes();
        populate_cube_vbo();
        configure_vbo_buffer();
        populate_first_texture_unit();
}

auto model_view_matrix()
{
        C3D_Mtx model_view;
        Mtx_Identity(&model_view);
        Mtx_Translate(&model_view, 0.0, 0.0, -5.0 + 2.5 * sinf(angle_x), true);
        Mtx_RotateX(&model_view, angle_x, true);
        Mtx_RotateY(&model_view, angle_y, true);
        return model_view;
}

auto rotate_cube()
{
        angle_x += M_PI / 180;
        angle_y += M_PI / 360;
}

auto compute_projection_matrix(const float interocular_distance)
{
        Mtx_PerspStereoTilt(&projection, C3D_AngleFromDegrees(40.0f), C3D_AspectRatioTop, 0.01f, 1000.0f,
                            interocular_distance, 2.0f, false);
}

auto update_uniforms(const C3D_Mtx &model_view)
{
        C3D_FVUnifMtx4x4(GPU_VERTEX_SHADER, uloc_projection, &projection);
        C3D_FVUnifMtx4x4(GPU_VERTEX_SHADER, uloc_model_view, &model_view);
        C3D_FVUnifMtx4x4(GPU_VERTEX_SHADER, uloc_material, &MATERIAL);
        C3D_FVUnifSet(GPU_VERTEX_SHADER, uloc_light_vec, 0.0f, 0.0f, -1.0f, 0.0f);
        C3D_FVUnifSet(GPU_VERTEX_SHADER, uloc_light_half_vec, 0.0f, 0.0f, -1.0f, 0.0f);
        C3D_FVUnifSet(GPU_VERTEX_SHADER, uloc_light_clr, 1.0f, 1.0f, 1.0f, 1.0f);
}

auto draw_vbo()
{
        C3D_DrawArrays(GPU_TRIANGLES, 0, CUBE_VERTICES_COUNT);
}

auto render_scene(const float interocular_distance)
{
        const auto model_view{model_view_matrix()};
        rotate_cube();
        compute_projection_matrix(interocular_distance);
        update_uniforms(model_view);
        draw_vbo();
}

auto free_texture()
{
        C3D_TexDelete(&kitten_tex);
}

auto free_vbo()
{
        linearFree(vbo_data);
}

auto free_shader_program()
{
        shaderProgramFree(&shader_program);
        DVLB_Free(vshader_dvlb);
}

auto deinitialize_scene()
{
        free_texture();
        free_vbo();
        free_shader_program();
}

auto initialize_graphics_unit()
{
        gfxInitDefault();
        consoleInit(GFX_BOTTOM, nullptr);
        gfxSet3D(true); // AUTOSTEREOSCOPIC 3D
        C3D_Init(C3D_DEFAULT_CMDBUF_SIZE);
        target_left = C3D_RenderTargetCreate(240, 400, GPU_RB_RGBA8, GPU_RB_DEPTH24_STENCIL8);
        C3D_RenderTargetSetOutput(target_left, GFX_TOP, GFX_LEFT, DISPLAY_TRANSFER_FLAGS);
        // AUTOSTEREOSCOPIC 3D
        target_right = C3D_RenderTargetCreate(240, 400, GPU_RB_RGBA8, GPU_RB_DEPTH24_STENCIL8);
        C3D_RenderTargetSetOutput(target_right, GFX_TOP, GFX_RIGHT, DISPLAY_TRANSFER_FLAGS);
}

auto render_frame(const float interocular_distance)
{
        C3D_FrameBegin(C3D_FRAME_SYNCDRAW);
        {
                C3D_RenderTargetClear(target_left, C3D_CLEAR_ALL, CLEAR_COLOR, 0);
                C3D_FrameDrawOn(target_left);
                render_scene(interocular_distance);
                // AUTOSTEREOSCOPIC 3D
                C3D_RenderTargetClear(target_right, C3D_CLEAR_ALL, CLEAR_COLOR, 0);
                C3D_FrameDrawOn(target_right);
                render_scene(-interocular_distance);
        }
        C3D_FrameEnd(0);
}

auto game_loop()
{
        printf("== This demo was built with CMake ==\n");
        printf("Best viewed with small IOD values.\n\n");
        auto former_slider = -1.0f;
        while (aptMainLoop())
        {
                hidScanInput();
                const auto kDown{hidKeysDown()};
                if (kDown & KEY_START)
                {
                        return; // to hbmenu
                }
                else
                {
                        // AUTOSTEREOSCOPIC 3D
                        const auto curr_slider = osGet3DSliderState();
                        if (curr_slider != former_slider)
                        {
                                printf("Interocular distance slider: %f\n", curr_slider);
                        }
                        former_slider = curr_slider;
                        render_frame(curr_slider);
                }
        }
        // to hbmenu
}

auto deinitialize_graphics_unit()
{
        C3D_Fini();
        gfxExit();
}

auto main() -> int
{
        initialize_graphics_unit();
        initialize_scene();

        game_loop();

        deinitialize_scene();
        deinitialize_graphics_unit();

        return 0; // to hbmenu
}
