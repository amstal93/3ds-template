#
# 3DS Template
#
# Holger Zahnleiter, 2021-04-16
#

.PHONY: all clean prepare build image push

all: clean prepare build

clean:
	rm -rf build

prepare:
	cmake -DCMAKE_TOOLCHAIN_FILE=cmake-support/DevkitArm3DS.cmake -S . -B build

build:
	cmake --build build
